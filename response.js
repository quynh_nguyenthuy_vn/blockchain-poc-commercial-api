function Response(code, message, data) {
    this.error_code = code;
    this.error_message = message;
    this.data = data;
}

module.exports = Response;