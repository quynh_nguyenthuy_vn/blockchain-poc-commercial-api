const { Gateway } = require('fabric-network');
const { config } = require('../config/config');
const CommercialPaper = require('../lib/paper.js');
const Response = require('../response');

var issue = async function(org, issuer, paperNumber, issueDateTime, maturityDateTime, faceValue) {
    const gateway = new Gateway();
    try {
        await gateway.connect(config[org].connectionProfile, config[org].connectionOptions);
        
        console.log('Use network channel: mychannel.');
        const network = await gateway.getNetwork('mychannel');

        // Get addressability to commercial paper contract
        console.log('Use org.papernet.commercialpaper smart contract.');
        const contract = await network.getContract('papercontract');

        // issue commercial paper
        console.log('Submit commercial paper issue transaction.');
        const issueResponse = await contract.submitTransaction('issue', issuer, paperNumber, issueDateTime, maturityDateTime, faceValue);

        // Disconnect from the gateway.
        await gateway.disconnect();

        // process response
        console.log(`Process issue transaction response ${issueResponse}`);

        let paper = CommercialPaper.fromBuffer(issueResponse);

        console.log(`${paper.issuer} commercial paper : ${paper.paperNumber} successfully issued for value ${paper.faceValue}`);
        console.log('Transaction complete.');
        return paper;
    } catch (error) {
        console.log(error.stack);
        return new Response('99', 'Issue failed');
    } finally {
        console.log('Disconnect from Fabric Gateway');
        await gateway.disconnect();
    }
}

var query = async function(org, issuer, paperNumber, owner, state) {
    const gateway = new Gateway();
    try {
        await gateway.connect(config[org].connectionProfile, config[org].connectionOptions);
        
        console.log('Use network channel: mychannel.');
        const network = await gateway.getNetwork('mychannel');

        // Get addressability to commercial paper contract
        console.log('Use org.papernet.commercialpaper smart contract.');
        const contract = await network.getContract('papercontract');

        // query commercial paper
        console.log('Commercial paper query transaction.');

        let queryResponse = '';
        if (issuer && paperNumber)
            queryResponse = await contract.evaluateTransaction('query', issuer, paperNumber);
        else if (issuer)
            queryResponse = await contract.evaluateTransaction('queryByIssuer', issuer);
        else if (owner)
            queryResponse = await contract.evaluateTransaction('queryByOwner', owner);
        else if (state)
            queryResponse = await contract.evaluateTransaction('queryByCurrentState', state);
        else
            queryResponse = await contract.evaluateTransaction('queryAll');

        // process response
        console.log(`Process query response: ${queryResponse}`);
        let paper = CommercialPaper.fromBuffer(queryResponse);
        return paper;
    } catch (error) {
        console.log(error.stack);
        return new Response('99', 'Query failed');
    } finally {
        console.log('Disconnect from Fabric Gateway');
        await gateway.disconnect();
    }
}

module.exports = { issue, query };