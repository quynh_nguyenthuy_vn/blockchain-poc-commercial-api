var express = require('express');
var bodyParser = require('body-parser');
var Response = require('./response');
const { initConfig } = require('./config/config');

var app = express();
var port = process.env.PORT || 3000;

var paperRouter = require('./routes/paper');

app.use('/', function(req, res, next) {
    console.log('Request url: ' + req.url);
    next();
})

app.use(bodyParser.json());

app.use('/paper', paperRouter);

initConfig();

app.listen(port);


