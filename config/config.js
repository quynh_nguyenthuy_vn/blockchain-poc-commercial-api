const yaml = require('js-yaml');
const { Wallets } = require('fabric-network');
const fs = require('fs');

let config = new Map();

async function initConfig() {
    config['MagnetoCorp'] = {
        connectionProfile: yaml.safeLoad(fs.readFileSync('./config/connection/connection-org2.yaml', 'utf8')),
        connectionOptions: {
            identity: 'isabella',
            wallet: await Wallets.newFileSystemWallet('./config/wallet'),
            discovery: { enabled:true, asLocalhost: true }
        }
    };
    
    config['DigiBank'] = {
        connectionProfile: yaml.safeLoad(fs.readFileSync('./config/connection/connection-org1.yaml', 'utf8')),
        connectionOptions: {
            identity: 'balaji',
            wallet: await Wallets.newFileSystemWallet('./config/wallet'),
            discovery: { enabled:true, asLocalhost: true }
        }
    };
}

module.exports = { config, initConfig };