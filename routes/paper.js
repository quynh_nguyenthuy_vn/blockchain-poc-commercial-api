var express = require('express');
var router = express.Router();
var { issue, query } = require('../logic/paper');
var Response = require('../response');

router.post('/issue', async function(req, res) {
    let { paperNumber, issueDateTime, maturityDateTime, faceValue } = req.body;
    let org = req.headers['org'];
    console.log(org);
    console.log(paperNumber, issueDateTime, maturityDateTime, faceValue);
    if (org === 'MagnetoCorp') {
        let response = await issue(org, org, paperNumber, issueDateTime, maturityDateTime, faceValue);
        res.json(response);
    } else
        res.status(403).send(new Response('03', 'Organization is not allowed to use this feature'));
});

router.post('/query', async function(req, res) {
    let { issuer, paperNumber, owner, state } = req.body;
    let org = req.headers['org'];
    console.log(org);
    console.log(issuer, paperNumber, owner, state);
    let response = await query(org, issuer, paperNumber, owner, state);
    res.json(response);
})

router.post('/buy', function(req, res) {
    res.send('Submit commercial paper buy transaction.');
})

router.post('/redeem', function(req, res) {
    res.send('Submit commercial paper redeem transaction.');
})
  
module.exports = router;